![Paggcerto](https://www.paggcerto.com.br/sistema/Content/img/logo.png)

# Desafio Android

A Paggcerto &eacute; uma empresa criada em 2013 que fornece aos seus clientes op&ccedil;&otilde;es para meios de pagamentos.
Estamos precisando de um app para realizar transa&ccedil;&otilde;es e-commerce via smartphone e por isso precisamos de ajuda.
O desafio &eacute; criar um app que traga algumas das funcionalidades de um e-commerce como listar estoque, realizar vendas, dar baixa no estoque, listar as vendas realizadas, etc...
Nossos produtos j&aacute; est&atilde;o dispon&iacute;veis na URL logo abaixo que voc&ecirc; pode consumir fazendo uma requisi&ccedil;&atilde;o GET.
Ao listar os produtos, apresente as seguintes informa&ccedil;&otilde;es:

  * O nome;
  * O pre&ccedil;o;
  * A quantidade;
  * A foto.

As vendas devem ser realizadas via cart&atilde;o de **cr&eacute;dito/d&eacute;bito** usando:

  * O n&uacute;mero do cart&atilde;o (entre 13 e 19 d&iacute;gitos);
  * O nome impresso no cart&atilde;o;
  * O vencimento (m&ecirc;s/ano);
  * CVV;
  * O valor da transa&ccedil;&atilde;o.

No momento da listagem das vendas realizadas, devem ser apresentados os seguintes dados:

  * O valor total da venda;
  * A data e hora;
  * Os 4 primeiros e 4 &uacute;ltimos d&iacute;gitos do cart&atilde;o;
  * O nome do comprador;
  * Os itens comprados com:
    * Quantidade;
    * Valor unit&aacute;rio;
    * Valor total.

# URL com os produtos

  * https://gitlab.com/programadorthi/desafio-paggcerto-android/raw/master/products.json

## Requisitos obrigat&oacute;rios

  * Android Studio e Gradle;
  * Java ou Kotlin;

### O que ser&aacute; avaliado

  * Padr&otilde;es de Projeto;
  * Performance;
  * Escrita de c&oacute;digo.

##### Obs.: Lembre que a vaga &eacute; para desenvolvedor, mas isso n&atilde;o significa que voc&ecirc; deva organizar os seus layouts/widgets de qualquer forma.
